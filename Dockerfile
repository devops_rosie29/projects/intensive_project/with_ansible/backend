FROM maven:3.6.3 AS build

WORKDIR /app
COPY . /app
RUN mvn clean package -Dmaven.test.skip=true

FROM adoptopenjdk/openjdk11:alpine-jre

WORKDIR /app
COPY --from=build /app/target/*.jar /app/student.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/student.jar"]
